#include <stdio.h>
#include <stdint.h>

/* AXI_GTY driver things*/
#define GTY_READ   0X01
#define GTY_WRITE  0X03
#define LTI_GPIO_R 0x05
#define LTI_GPIO_W 0x07

typedef enum
{
    GPI_tx_reset_done = 0,
    GPI_rx_reset_done,
    GPI_decoder_aligned,
    GPI_NC_1 // Not connected
} GPI_GTY_t;

typedef enum
{
    GPO_NC0 = 0, // Not connected
    GPO_ch0_eyescanreset,
    GPO_reset_tx_datapath,
    GPO_reset_rx_datapath
} GPO_GTY_t;

typedef struct _reg_write_arg
{
    uint32_t addr;
    uint32_t val;
} reg_write_arg_t;

// https://support.xilinx.com/s/article/000033098?language=en_US

#define CHx_PIPE_CTRL_CFG7(channel)  ((0x0CA4 +(channel<<8) << 2) 
#define CH0_PIPE_CTRL_CFG7           (0x0CA4 << 2)

/* Eyescan registers */
#define CHx_EYESCAN_CFG0(channel)    ((0x0C7B +(channel<<8) << 2) 
#define CH0_EYESCAN_CFG0             (0x0C7B << 2)

/* ES_QUAL_MASK per channel*/
#define CH0_EYESCAN_CFG6     (0x0C81 << 2)
#define CH0_EYESCAN_CFG7     (0x0C82 << 2)
#define CH0_EYESCAN_CFG8     (0x0C83 << 2)
#define CH0_EYESCAN_CFG9     (0x0C84 << 2)
#define CH0_EYESCAN_CFG10    (0x0C85 << 2)

#define CH1_EYESCAN_CFG6     (0x0D81 << 2)
#define CH1_EYESCAN_CFG7     (0x0D82 << 2)
#define CH1_EYESCAN_CFG8     (0x0D83 << 2)
#define CH1_EYESCAN_CFG9     (0x0D84 << 2)
#define CH1_EYESCAN_CFG10    (0x0D85 << 2)

#define CH2_EYESCAN_CFG6     (0x0E81 << 2)
#define CH2_EYESCAN_CFG7     (0x0E82 << 2)
#define CH2_EYESCAN_CFG8     (0x0E83 << 2)
#define CH2_EYESCAN_CFG9     (0x0E84 << 2)
#define CH2_EYESCAN_CFG10    (0x0E85 << 2)

#define CH3_EYESCAN_CFG6     (0x0F81 << 2)
#define CH3_EYESCAN_CFG7     (0x0F82 << 2)
#define CH3_EYESCAN_CFG8     (0x0F83 << 2)
#define CH3_EYESCAN_CFG9     (0x0F84 << 2)
#define CH3_EYESCAN_CFG10    (0x0F85 << 2)

#define ES_QUAL_MASK0_CH(channel)    (CH0_EYESCAN_CFG6  + (channel << 10))
#define ES_QUAL_MASK1_CH(channel)    (CH0_EYESCAN_CFG7  + (channel << 10))
#define ES_QUAL_MASK2_CH(channel)    (CH0_EYESCAN_CFG8  + (channel << 10))
#define ES_QUAL_MASK3_CH(channel)    (CH0_EYESCAN_CFG9  + (channel << 10))
#define ES_QUAL_MASK4_CH(channel)    (CH0_EYESCAN_CFG10 + (channel << 10))


/* ES_SDATA_MASK per channel*/
#define CH0_EYESCAN_CFG11    (0x0C86 << 2)
#define CH0_EYESCAN_CFG12    (0x0C87 << 2)
#define CH0_EYESCAN_CFG13    (0x0C88 << 2)
#define CH0_EYESCAN_CFG14    (0x0C89 << 2)
#define CH0_EYESCAN_CFG15    (0x0C8A << 2)

#define CH1_EYESCAN_CFG11    (0x0D86 << 2)
#define CH1_EYESCAN_CFG12    (0x0D87 << 2)
#define CH1_EYESCAN_CFG13    (0x0D88 << 2)
#define CH1_EYESCAN_CFG14    (0x0D89 << 2)
#define CH1_EYESCAN_CFG15    (0x0D8A << 2)

#define CH2_EYESCAN_CFG11    (0x0E86 << 2)
#define CH2_EYESCAN_CFG12    (0x0E87 << 2)
#define CH2_EYESCAN_CFG13    (0x0E88 << 2)
#define CH2_EYESCAN_CFG14    (0x0E89 << 2)
#define CH2_EYESCAN_CFG15    (0x0E8A << 2)

#define CH3_EYESCAN_CFG11    (0x0F86 << 2)
#define CH3_EYESCAN_CFG12    (0x0F87 << 2)
#define CH3_EYESCAN_CFG13    (0x0F88 << 2)
#define CH3_EYESCAN_CFG14    (0x0F89 << 2)
#define CH3_EYESCAN_CFG15    (0x0F8A << 2)

#define ES_SDATA_MASK0(channel)    (CH0_EYESCAN_CFG11 + (channel << 10))
#define ES_SDATA_MASK1(channel)    (CH0_EYESCAN_CFG12 + (channel << 10))
#define ES_SDATA_MASK2(channel)    (CH0_EYESCAN_CFG13 + (channel << 10))
#define ES_SDATA_MASK3(channel)    (CH0_EYESCAN_CFG14 + (channel << 10))
#define ES_SDATA_MASK4(channel)    (CH0_EYESCAN_CFG15 + (channel << 10))


#define CH0_EYESCAN_CFG16    (0x0C8B << 2)
#define CH1_EYESCAN_CFG16    (0x0D8B << 2)
#define CH2_EYESCAN_CFG16    (0x0E8B << 2)
#define CH3_EYESCAN_CFG16    (0x0F8B << 2)

#define CH0_ES_CONTROL_STATUS    (0x085B << 2)
#define CH1_ES_CONTROL_STATUS    (0x095B << 2)
#define CH2_ES_CONTROL_STATUS    (0x0A5B << 2)
#define CH3_ES_CONTROL_STATUS    (0x0B5B << 2)

#define CH0_ES_ERROR_COUNT    (0x085A << 2)
#define CH1_ES_ERROR_COUNT    (0x095A << 2)
#define CH2_ES_ERROR_COUNT    (0x0a5A << 2)
#define CH3_ES_ERROR_COUNT    (0x0b5A << 2)

#define ES_HORZ_OFFSET_MIN_COUNT     -32
#define ES_HORZ_OFFSET_CENTER_COUNT  0
#define ES_HORZ_OFFSET_MAX_COUNT     32

#define ES_VERT_OFFSET_MIN_COUNT     -124
#define ES_VERT_OFFSET_CENTER_COUNT  0
#define ES_VERT_OFFSET_MAX_COUNT     124
