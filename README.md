# Knypaegje

Knypaegje is a program that runs on the ARM processor of the Versal FPGA and communicates with the GTYe5 transceivers to generate eye diagrams. It measures a small number of eyes (currently 10). After these measurements, it checks to see if the current eye opening has met the threshold of being open enough. If this is not the case, it resets the RX of the transceiver until this threshold is met.

## Dependencies
- [AXI GTY kernel module](https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/gty-axi-driver)

- [Petalinux environnement](https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/flx-petalinux/-/tree/FLX-2438_petalinux_knypaegje?ref_type=heads)
