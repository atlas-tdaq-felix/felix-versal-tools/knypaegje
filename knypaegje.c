/*
    Knypaegje, a sequence of measuring eye diagrams to determine the startup phase of the transceiver.
    Designed for Versal GTYe5 transceivers.

    Eye diagram sequence based on: // https://support.xilinx.com/s/article/000033098?language=en_US
*/

#include <stdio.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#include "AXI_GTY_regs.h"

#define MAX_EYE_COUNT 10

FILE *device_driver;

uint32_t AXI_read(uint32_t addr);
void AXI_write(uint32_t addr, uint32_t data);

uint32_t GPIO_read(uint32_t addr);
void GPIO_write(uint32_t addr, uint32_t data);

uint8_t realignment(void);
uint32_t runeye(void);
void reset_LTI_RX(void);

int cmpfunc(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

int main(int argc, char *argv[])
{
    device_driver = fopen("/dev/AXI_GTY", "rw");

    uint32_t open_areas[MAX_EYE_COUNT];

    uint32_t current_open_area = 0;

    for (uint8_t i = 0; i < MAX_EYE_COUNT; i++)
    {
        reset_LTI_RX();

        // printf("Meas eye nr:%d\n", i);
        open_areas[i] = runeye();
        current_open_area = open_areas[i];
    }
    
    // TODO: Check if there is enought spread
    qsort(open_areas, MAX_EYE_COUNT, sizeof(int), cmpfunc);
    // printf("Open areas:\n");

    // for (uint8_t i = 0; i < MAX_EYE_COUNT; i++)
    // {
    //     printf("%d | ", open_areas[i]);
    // }
    // printf("\n");
        uint16_t measured_spread = open_areas[MAX_EYE_COUNT-1] - open_areas[0];


    while (current_open_area < open_areas[MAX_EYE_COUNT - 2])
    {
        reset_LTI_RX();
        current_open_area = runeye();
        // printf("Current open eye = %d. Need %d\n", current_open_area, open_areas[MAX_EYE_COUNT - 2]);
    }
        printf("%d,%d",measured_spread,current_open_area);
        for (uint8_t i = 0; i < MAX_EYE_COUNT; i++)
    {
        printf(",%d", open_areas[i]);
    }

    // printf("Done\n");
    fclose(device_driver);
}

uint32_t AXI_read(uint32_t addr)
{
    reg_write_arg_t test;
    test.addr = addr;
    ioctl(fileno(device_driver), GTY_READ, &test);
    return test.val;
}

void AXI_write(uint32_t addr, uint32_t data)
{
    reg_write_arg_t test;
    test.addr = addr;
    test.val = data;
    ioctl(fileno(device_driver), GTY_WRITE, &test);
}

uint32_t GPIO_read(uint32_t addr)
{
    reg_write_arg_t test;
    test.addr = 0;
    ioctl(fileno(device_driver), LTI_GPIO_R, &test);
    return (test.val & (1 << addr)) >> addr;
}

void GPIO_write(uint32_t addr, uint32_t data)
{
    reg_write_arg_t test;
    test.addr = 0;
    test.val = data << addr;
    // TODO: RmW
    ioctl(fileno(device_driver), LTI_GPIO_W, &test);
}

void reset_LTI_RX(void)
{
    // printf("Resetting LTI RX\n");
    GPIO_write(3, 1);
    usleep(1); // Seems nececery at the moment (05-11-2024)
    GPIO_write(3, 0);
    do
    {
    } while (!(GPIO_read(1) & GPIO_read(2))); // Wait for the transceiver and manual decoder to be ready
    // printf("Reset done\n");
}

// TODO: Delay comments
uint8_t realignment(void)
{
    uint32_t AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);

    AXI_ret_val &= 0xFFFFF000;
    AXI_ret_val |= 0x880;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // Delay?
    GPIO_write(GPO_ch0_eyescanreset, 1);

    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFFFFF000;
    AXI_ret_val |= 0x800;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);
    // Delay?
    GPIO_write(GPO_ch0_eyescanreset, 0);
    // Delay?

    // step_5:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val &= 0xFFFFE003;
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);

    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFFFFF000;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_6:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 1 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_7:
    AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    while ((AXI_ret_val & 0xe) != 0x4)
    {
        AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    }

    // step_8:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 0 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    //  step_9:
    AXI_ret_val = AXI_read(CH0_ES_ERROR_COUNT);
    AXI_ret_val &= 0xFFFF;
    return AXI_ret_val;
}

int singlepoint(int x, int y)
{
    uint32_t AXI_ret_val;
    // if (y< ES_VERT_OFFSET_MIN_COUNT || y > ES_VERT_OFFSET_MAX_COUNT || x< ES_HORZ_OFFSET_MIN_COUNT || x > ES_HORZ_OFFSET_MAX_COUNT)
    // {
    //     //printf("Illegal x:%i or y:%i\n", x,y);
    //     return 0xFFFF;
    // }
    // if (y< ES_VERT_OFFSET_MIN_COUNT )
    //     y = ES_VERT_OFFSET_MIN_COUNT;
    // if (y< ES_VERT_OFFSET_MAX_COUNT )
    //     y = ES_VERT_OFFSET_MAX_COUNT;
    // if (x< ES_HORZ_OFFSET_MIN_COUNT )
    //     x = ES_HORZ_OFFSET_MIN_COUNT;
    // if (x< ES_HORZ_OFFSET_MAX_COUNT )
    //     x = ES_HORZ_OFFSET_MAX_COUNT;

    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val &= 0xFFFFFC03;
    AXI_ret_val |= ((y & 0x80) << 2) | ((abs(y) & 0x7F) << 2);
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);

    // step 10
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFFFFF000;
    AXI_ret_val |= (x & 0xFFF);
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_11:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 1 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step 12:
    AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    while ((AXI_ret_val & 0xe) != 0x4)
    {
        AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    }

    // step_13:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 0 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    AXI_ret_val = AXI_read(CH0_ES_ERROR_COUNT);

    // printf("x: %i, y: %i, err: %i\n", x,y,AXI_ret_val&0xFFFF);

    return AXI_ret_val & 0xFFFF; // return error count
}

uint32_t runeye(void)
{
    uint32_t result;
    uint32_t counter;
    uint32_t open_area = 0;

    // Starting manual eye scan
    uint32_t AXI_ret_val;

    // Step 1: initialization:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val |= 0b11 << 23;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // Set register EYESCAN_CLK_PHASE_SAMPLE, AM002 does not explain what this does (27-08-2024).
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val &= 0xFFFF7FFF; // Set bit 15 to 0
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);
    usleep(100);
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val |= 0b1 << 15; // Set bit 15 to 1
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);
    usleep(100);
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val &= 0xFFFF7FFF; // Set bit 15 to 0
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);

    // // reset:
    // AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    // AXI_ret_val = 0x01800000;
    // AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_3:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF804FFF;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_4:
    AXI_write(CH0_EYESCAN_CFG6, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG7, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG8, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG9, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG10, UINT32_MAX);

    AXI_write(CH0_EYESCAN_CFG11, 0);
    AXI_write(CH0_EYESCAN_CFG12, 0xFFFFFF00);
    AXI_write(CH0_EYESCAN_CFG13, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG14, UINT32_MAX);
    AXI_write(CH0_EYESCAN_CFG15, UINT32_MAX);

    // step_5:
    // STEP5:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    AXI_ret_val &= 0xFFFFE003;
    AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);

    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFFFFF000;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_6:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 1 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    // step_7:
    AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    while ((AXI_ret_val & 0xe) != 0x4)
    {
        AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    }

    // step_8:
    AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    AXI_ret_val &= 0xFF81FFFF;
    AXI_ret_val |= 0 << 17;
    AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    //  step_9:
    AXI_ret_val = AXI_read(CH0_ES_ERROR_COUNT);
    if ((AXI_ret_val & 0xFFFF) > 1)
    {
        printf("Realign\n");
        counter = 0;
        do
        {
            result = realignment();
        } while (result && counter++ < 1000);
        if (counter >= 1000)
        {
            printf("Error: realigned 1000 times\n");
            // TODO : Hardreset if realignment keeps failing
            // printf("Resetting LTI\n");
            // GPIO_write(GPO_reset_rx_datapath, 1);
            // GPIO_write(GPO_reset_rx_datapath, 0);
        }
    }

    // step 10 - 13
    //     for (int y = ES_VERT_OFFSET_MIN_COUNT; y <= ES_VERT_OFFSET_MAX_COUNT; y++)
    //     {
    //         AXI_ret_val = AXI_read(CH0_EYESCAN_CFG16);
    //         AXI_ret_val &= 0xFFFFFC03;
    //         AXI_ret_val |= ((y & 0x80) << 2) | ((abs(y) & 0x7F) << 2);
    //         AXI_write(CH0_EYESCAN_CFG16, AXI_ret_val);

    //         for (int x = ES_HORZ_OFFSET_MIN_COUNT; x <= ES_HORZ_OFFSET_MAX_COUNT; x++)
    //         {
    //             // step 10
    //             AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    //             AXI_ret_val &= 0xFFFFF000;
    //             AXI_ret_val |= (x & 0xFFF);
    //             AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    //             // step_11:
    //             AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    //             AXI_ret_val &= 0xFF81FFFF;
    //             AXI_ret_val |= 1 << 17;
    //             AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    //             // step 12:
    //             AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    //             while ((AXI_ret_val & 0xe) != 0x4)
    //             {
    //                 AXI_ret_val = AXI_read(CH0_ES_CONTROL_STATUS);
    //             }

    //             // step_13:
    //             AXI_ret_val = AXI_read(CH0_EYESCAN_CFG0);
    //             AXI_ret_val &= 0xFF81FFFF;
    //             AXI_ret_val |= 0 << 17;
    //             AXI_write(CH0_EYESCAN_CFG0, AXI_ret_val);

    //             AXI_ret_val = AXI_read(CH0_ES_ERROR_COUNT);

    //             if ((AXI_ret_val & 0xFFFF) == 0)
    //     {
    //                 open_area++;
    //                 }
    //                 }
    //             }
    //     return open_area;
    // }
    // step 10 - 13

    /*for (int y = ES_VERT_OFFSET_MIN_COUNT; y <= ES_VERT_OFFSET_MAX_COUNT; y++)
    {
        for (int x = ES_HORZ_OFFSET_MIN_COUNT; x <= ES_HORZ_OFFSET_MAX_COUNT; x++)
        {

            if(singlepoint(x,y) == 0)
            {
                open_area++;
            }
        }
    }*/

    /// Binary search on X
    /*int xleft, xright, xleftopen, xrightopen;
    for (int y = ES_VERT_OFFSET_MIN_COUNT; y <= ES_VERT_OFFSET_MAX_COUNT; y++)
    {
        xleft=0;
        xright=0;
        for (int xstep=ES_HORZ_OFFSET_MAX_COUNT; xstep>=1; xstep>>=1)
        {

            if(singlepoint(xleft,y) == 0)
            {
                xleft -= xstep;
            }
            else
            {
                xleft += xstep;
            }
            if(singlepoint(xright, y) ==0)
            {
                xright += xstep;
            }
            else
            {
                xright -= xstep;
            }
            if(xleft < ES_HORZ_OFFSET_MIN_COUNT)
            {
                printf("Error, xleft: %i\n", xleft);
                xleft = ES_HORZ_OFFSET_MIN_COUNT;
            }
            if(xleft > 0)
            {
                printf("Error, xleft: %i\n", xleft);
                xleft = 0;
            }
            if(xright > ES_HORZ_OFFSET_MAX_COUNT)
            {
                printf("Error, xright: %i\n", xright);
                xright = ES_HORZ_OFFSET_MAX_COUNT;
            }
            if(xright < 0)
            {
                printf("Error, xright: %i\n", xright);
                xright = 0;
            }
        }
        open_area += (xright - xleft);
    }*/

    // Binary search on Y
    int ytop, ybot;
    for (int x = ES_HORZ_OFFSET_MIN_COUNT; x < ES_HORZ_OFFSET_MAX_COUNT; x++)
    {
        ytop = 0;
        ybot = 0;
        for (int ystep = 64; ystep >= 1; ystep >>= 1)
        {
            if ((ytop >= ES_VERT_OFFSET_MAX_COUNT) && (ybot <= ES_VERT_OFFSET_MIN_COUNT))
            {
                break;//printf("Top reached %d\n", ytop);
            }
            // if (ybot < ES_VERT_OFFSET_MIN_COUNT)
            // {
            //     break;//printf("Bottom reached %d\n", ybot);
            // }

            if (singlepoint(x, ytop) == 0)
            {
                ytop -= ystep;
            }
            else
            {
                ytop += ystep;
            }
            if (singlepoint(x, ybot) == 0)
            {
                ybot += ystep;
            }
            else
            {
                ybot -= ystep;
            }
            // if(ytop < ES_VERT_OFFSET_MIN_COUNT)
            // {
            //     printf("Error, ytop: %i\n", ytop);
            //     ytop = ES_VERT_OFFSET_MIN_COUNT;
            // }
            /*if(ytop > 0)
            {
                 //printf("Error, ytop: %i\n", ytop);
                 ytop = 0;
            }*/
            //
            // if(ybot > ES_VERT_OFFSET_MAX_COUNT)
            // {
            //     printf("Error, ybot: %i\n", ybot);
            //     ybot = ES_VERT_OFFSET_MAX_COUNT;
            // }
            // if(ybot < 0)
            // {
            //     //printf("Error, ybot: %i\n", ybot);
            //     ybot = 0;
            // }
        }
        if (ytop > 0)
            ytop = 0;
        if (ybot < 0)
            ybot = 0;
        // if(ybot>ES_VERT_OFFSET_MAX_COUNT)ybot=ES_HORZ_OFFSET_MAX_COUNT;
        // if(ytop<ES_VERT_OFFSET_MIN_COUNT)ytop=ES_HORZ_OFFSET_MIN_COUNT;

        open_area += (ybot - ytop);
    }

    return open_area;
}
